/* eslint-disable object-property-newline */
/* eslint-disable key-spacing */
/* eslint-disable block-spacing */
/* eslint-disable brace-style */
/* global chrome */

class VisualController {
	constructor() {
		if (VisualController.instance) {
			return VisualController.instance;
		}

		VisualController.instance = this;
		this.container = null;
		this.loader = null;
		this.saveBtn = null;
		this.stashBtn = null;
		this.popBtn = null;
		this.popAllBtn = null;
		this.list = null;
		this.youngest = "";
		this.vocabulary = {
			"remove": (function() {
				if ("ru-RU".includes(navigator.language)) {
					return "Удалить сессию";
				}
					return "Remove session";
			}()),
			"areremove": (function() {
				if ("ru-RU".includes(navigator.language)) {
					return "Удалить сессию?";
				}
					return "Remove session?";
			}()),
			"yes": (function() {
				if ("ru-RU".includes(navigator.language)) {
					return "да";
				}
					return "yes";
			}()),
			"no": (function() {
				if ("ru-RU".includes(navigator.language)) {
					return "нет";
				}
					return "no";
			}())
		}
	}

	init() {
		this.container = document.querySelector(".container");
		this.loader = document.querySelector(".loader");
		this.saveBtn = document.querySelector('.top-button.button-save');
		this.stashBtn = document.querySelector('.top-button.button-stash');
		this.popBtn = document.querySelector('.top-button.button-pop');
		this.popAllBtn = document.querySelector('.top-button.button-pop-all');
		this.list = document.getElementById("list");
		this.regListeners();
		this.createList();
	}

	regListeners() {
		chrome.runtime.onMessage.addListener((message) => {
			if (message.type === "reload") {
				const name = String(message.key);
				chrome.storage.sync.get(name, (r) => {
					this.createListElement(name, r[name]);
					this.checkYoungest(name);
				});
			} else if (message.type === "isRemove") {
				if (String(message.key) === this.youngest) {
					this.youngest = "";
					chrome.storage.sync.get((r) => {
						if (r) {
							for (let i = Object.keys(r).length; i > 0;--i) {
								const name = Object.keys(r)[i - 1];
								this.checkYoungest(name);
							}
						}
					});
				}
			}
		});

		//onclick buttons
		// eslint-disable-next-line max-statements-per-line
		this.saveBtn ? this.saveBtn.onclick = () => {chrome.runtime.sendMessage({'type': "save"});} : console.error("saveBtn is null");
		// eslint-disable-next-line max-statements-per-line
		this.stashBtn ? this.stashBtn.onclick = () => {chrome.runtime.sendMessage({'type': "stash"});} : console.error("stashBtn is null");
		// eslint-disable-next-line max-statements-per-line
		this.popBtn ? this.popBtn.onclick = () => {chrome.runtime.sendMessage({'type': "pop", "arg" : this.youngest});} : console.error("popBtn is null");
		// eslint-disable-next-line max-statements-per-line
		this.popAllBtn ? this.popAllBtn.onclick = () => {chrome.runtime.sendMessage({'type': "popAll"});} : console.error("popAllBtn is null");
	}

	// eslint-disable-next-line class-methods-use-this
	initDrugnDropListeners(listElem) {
		// eslint-disable-next-line no-nested-ternary
		const type = listElem.parentElement.classList.contains('.dropdown-menu') ? 'site' : listElem.parentElement.id === 'list' ? 'session' : null;
		if (type) {
			listElem.onmousedown = (event) => {
				const shiftX = event.pageX - listElem.getBoundingClientRect().left + pageXOffset;
				const shiftY = event.pageY - listElem.getBoundingClientRect().top + pageYOffset;
				listElem.style.position = 'absolute';
				listElem.style.zIndex = 1000;
				document.body.append(listElem);
				// listElem.style.left = event.pageX - shiftX + 'px';
				// listElem.style.top = event.pageY - shiftY + 'px';

				function onMouseMove(event) {
					// listElem.style.left = event.pageX - shiftX + 'px';
					listElem.style.top = event.pageY - shiftY + 'px';
				}

				document.addEventListener('mousemove', onMouseMove);

				listElem.onmouseup = () => {
					document.removeEventListener('mousemove', onMouseMove);
					listElem.onmouseup = null;
				};
			}
			listElem.ondragstart = () => false;
		} else {
			console.error('WTF?', listElem);

		}

		// element.removeAttribute("style")
// 		// потенциальная цель переноса, над которой мы пролетаем прямо сейчас
// let currentDroppable = null;

// function onMouseMove(event) {
//   moveAt(event.pageX, event.pageY);

//   ball.hidden = true;
//   let elemBelow = document.elementFromPoint(event.clientX, event.clientY);
//   ball.hidden = false;

//   // событие mousemove может произойти и когда указатель за пределами окна
//   // (мяч перетащили за пределы экрана)

//   // если clientX/clientY за пределами окна, elementFromPoint вернёт null
//   if (!elemBelow) return;

//   // потенциальные цели переноса помечены классом droppable (может быть и другая логика)
//   let droppableBelow = elemBelow.closest('.droppable');

//   if (currentDroppable != droppableBelow) {
//     // мы либо залетаем на цель, либо улетаем из неё
//     // внимание: оба значения могут быть null
//     //   currentDroppable=null,
//     //     если мы были не над droppable до этого события (например, над пустым пространством)
//     //   droppableBelow=null,
//     //     если мы не над droppable именно сейчас, во время этого события

//     if (currentDroppable) {
//       // логика обработки процесса "вылета" из droppable (удаляем подсветку)
//       leaveDroppable(currentDroppable);
//     }
//     currentDroppable = droppableBelow;
//     if (currentDroppable) {
//       // логика обработки процесса, когда мы "влетаем" в элемент droppable
//       enterDroppable(currentDroppable);
//     }
//   }
// }
	}

	enableLoader() {
		this.container.style.display = "none";
		this.loader.style.display = "";
	}

	disableLoader() {
		this.container.style.display = "";
		this.loader.style.display = "none"
	}

	createList() {
		chrome.storage.sync.get((r) => {
			if (r) {
				for (let i = Object.keys(r).length; i > 0;--i) {
					const name = Object.keys(r)[i - 1];
					this.createListElement(name, r[name]);
					this.checkYoungest(name);
				}
			}
			this.disableLoader();
		});
	}

	checkYoungest(name) {
		if (this.youngest.length) {
			if (Number.parseInt(this.youngest, 10) < Number.parseInt(name, 10)) {
				this.youngest = String(name);
			}
		} else {
			this.youngest = String(name);
		}
	}

	createListElement(name, obj) {
		const data = JSON.parse(obj);
		const liTag = document.createElement("li");
		liTag.classList.add("dropdown");
		this.list.appendChild(liTag);
		this.initDrugnDropListeners(liTag);
		const aTag = document.createElement("a");
		aTag.href = "#";
		aTag.classList.add("dropdown-a");
		aTag.setAttribute("data", name);
		const d = new Date(Number.parseInt(name, 10));
		if (data.discr.length) {
			aTag.textContent = data.discr;
		} else {
			aTag.textContent = `${d.getDate()}/${d.getMonth() + 1}/${d.getFullYear()} ${d.getHours()}:${("0" + d.getMinutes()).substr(-2)}`;
		}
		aTag.title = `${d.getDate()}/${d.getMonth() + 1}/${d.getFullYear()} ${d.getHours()}:${("0" + d.getMinutes()).substr(-2)}`;
		const editBut = document.createElement("button");
		editBut.classList.add("edi-but", "sub-but");
		editBut.title = "rename";
		editBut.onclick = (e) => {
			if (e.target.parentElement.getElementsByTagName("input").length) {
				return;
			}
			const inp = document.createElement("input");
			e.target.parentElement.appendChild(inp);
			inp.value = e.target.parentElement.firstChild.textContent;
			e.stopPropagation();
			inp.focus();
			inp.select();
			inp.onchange = (e) => {
				VisualController.rename(e.target);
			};
			inp.onkeypress = (e) => {
				if (e.keyCode === 13) {
					VisualController.rename(e.target);
				}
			}
		}

		aTag.appendChild(editBut);
		const popBut = document.createElement("button");
		popBut.classList.add("pop-but", "sub-but");
		popBut.title = "pop session";
		popBut.onclick = (e) => {
			chrome.runtime.sendMessage({
				'type': "pop",
				'arg': e.target.parentElement.getAttribute("data")
			});
			e.stopPropagation();
			e.target.parentElement.parentElement.classList.add("removing");
			e.target.parentElement.parentElement.addEventListener('webkitAnimationEnd', function(e) {
				e.target.remove()
			});
		}
		aTag.appendChild(popBut);

		const remBut = document.createElement("button");
		remBut.classList.add("rem-but", "sub-but");
		remBut.title = this.vocabulary.remove;
		remBut.onclick = (e) => {
			this.createDialog(this.vocabulary.areremove, (flag) => {
				if (flag) {
					chrome.runtime.sendMessage({
						'type': "remove",
						'arg': e.target.parentElement.getAttribute("data")
					});
					e.stopPropagation();
					e.target.parentElement.parentElement.classList.add("removing");
					e.target.parentElement.parentElement.addEventListener('webkitAnimationEnd', function(e) {
						e.target.remove();
					});
				}
			});
		}
		aTag.appendChild(remBut);

		const restBut = document.createElement("button");
		restBut.classList.add("rest-but", "sub-but");
		restBut.title = "restore session (not remove)";
		restBut.onclick = (e) => {
			chrome.runtime.sendMessage({
				'type': "restore",
				'arg': e.target.parentElement.getAttribute("data")
			});
			e.stopPropagation();
		}
		aTag.appendChild(restBut);

		aTag.onclick = (e) => {
			if (e.target.nextElementSibling === null) {
				return;
			}
			if (!e.target.nextElementSibling.classList.toggle("open")) {
				e.target.nextElementSibling.classList.toggle("hide");
				setTimeout(() => {
					e.target.nextElementSibling.classList.toggle("hide");
				}, 700);
			}
		}
		liTag.appendChild(aTag);
		const ulTag = document.createElement("ul");
		ulTag.classList.add("dropdown-menu");
		liTag.appendChild(ulTag);
		for (const obj of data.data) {
			ulTag.appendChild(VisualController.createSubListElement(obj));
		}
	}

	static rename(elem) {
		const text = elem.value;
		if (elem.parentElement.firstChild.textContent !== text) {
			if (text === "") {
				const d = new Date(Number.parseInt(elem.parentElement.getAttribute("data"), 10));
				elem.parentElement.firstChild.textContent = `${d.getDate()}/${d.getMonth() + 1}/${d.getFullYear()} ${d.getHours()}:${("0" + d.getMinutes()).substr(-2)}`;
			} else {
				elem.parentElement.firstChild.textContent = text;
			}
			chrome.runtime.sendMessage({
				'type': "discr",
				'arg': {
					'list': elem.parentElement.getAttribute("data"),
					'discr': text
				}
			});
		}
		elem.classList.add("removing");
		elem.addEventListener('webkitAnimationEnd', function(e) {
			e.target.remove()
		});
	}

	static createSubListElement(obj) {
		const liTag = document.createElement("li");
		const aTag = document.createElement("a");
		aTag.title = aTag.href = obj.url;
		liTag.appendChild(aTag);
		if (obj.favIconUrl && obj.favIconUrl.length) {
			const img = document.createElement("img");
			img.src = obj.favIconUrl;
			aTag.appendChild(img);
		}
		const text = document.createElement("text");
		text.textContent = obj.title;
		aTag.onclick = (e) => {
			let href = "";
			if (e.target.tagName === "A") {
				[href] = e.target;
			} else if (e.target.parentElement.tagName === "A") {
				[href] = e.target.parentElement;
			} else {
				console.error("tagName !==A");
				return;
			}
			if (e.ctrlKey) {
				chrome.tabs.create({
					'url': href,
					'active': false
				});
			} else {
				chrome.tabs.create({
					'url': href
				});
			}
		};
		aTag.appendChild(text);
		const remBut = document.createElement("button");
		remBut.classList.add("rem-but", "sub-but");
		remBut.title = "remove tab";
		remBut.onclick = (e) => {
			chrome.runtime.sendMessage({
				'type': "update",
				'arg': {
					'list': e.target.parentElement.parentElement.parentElement.previousElementSibling.getAttribute("data"),
					'url': e.target.parentElement.href
				}
			});
			e.stopPropagation();
			e.target.parentElement.parentElement.classList.add("removing");
			e.target.parentElement.parentElement.addEventListener('webkitAnimationEnd', function (e) {
				e.target.remove()
			});
			if (e.target.parentElement.parentElement.parentElement.childElementCount <= 1) {
				e.target.parentElement.parentElement.parentElement.parentElement.classList.add("removing");
				e.target.parentElement.parentElement.parentElement.parentElement.addEventListener('webkitAnimationEnd', function (e) {
					e.target.remove()
				});
			}
		}
		aTag.appendChild(remBut);
		return liTag;
	}

	createDialog(quastion, callback) {
		const curtain = document.body.appendChild(document.createElement("div"));
		curtain.classList.add("curtain");
		const textContainer = curtain.appendChild(document.createElement("div"));
		textContainer.textContent = quastion;
		const buttonContainer = curtain.appendChild(document.createElement("div"));
		const yesBtn = buttonContainer.appendChild(document.createElement("button"));
		yesBtn.classList.add("top-button", "answer-btns");
		yesBtn.textContent = this.vocabulary.yes;
		yesBtn.onclick = () => {
			curtain.remove();
			callback(true);
		};
		const noBtn = buttonContainer.appendChild(document.createElement("button"));
		noBtn.classList.add("top-button", "answer-btns");
		noBtn.textContent = this.vocabulary.no;
		noBtn.onclick = () => {
			curtain.remove();
			callback(false);
		};
	}
}

// eslint-disable-next-line no-unused-vars
var VC = new VisualController();
VC.init();