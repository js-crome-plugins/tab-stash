/* global chrome */

class BackgroundApp {
	constructor() {

		if (BackgroundApp.instance) {
			return BackgroundApp.instance;
		}

		BackgroundApp.instance = this;
		this.counterOfMenuItems = 0;
		this.vocabulary = {
			"add": (function() {
				if ("ru-RU".includes(navigator.language)) {
					return "Добавить в:";
				}
					return "Add to:";
			}()),
			"main": (function() {
				if ("ru-RU".includes(navigator.language)) {
					return "Панель управления";
				}
					return "Main panel";
			}()),
			"question": (function() {
				if ("ru-RU".includes(navigator.language)) {
					return "Вы хотите удалить все спрятанные страницы?";
				}
					return "Do you want to remove all pages from your stashes?";
			}())
		};

		//init listener
		chrome.runtime.onMessage.addListener((message) => {
			switch (message.type) {
				case "stash":
					this.stash();
					break;
				case "save":
					this.save();
					break;
				case "pop":
					BackgroundApp.pop(message.arg);
					break;
				case "restore":
					BackgroundApp.restore(message.arg);
					break;
				case "popAll":
					BackgroundApp.popAll();
					break;
				case "remove":
					this.removeSession(message.arg);
					break;
				case "update":
					BackgroundApp.updateSession(message.arg);
					break;
				case "discr":
					BackgroundApp.discript(message.arg);
					break;
				default:
					console.error(message, "message is not find");
			}
		});

		chrome.contextMenus.onClicked.addListener((info, tab) => {
			BackgroundApp.updateSession({
				"list": info.menuItemId,
				"insert": {
					"url": tab.url,
					"title": tab.title,
					"favIconUrl": tab.favIconUrl
				}
			});
		});

		chrome.runtime.onInstalled.addListener(() => {
			chrome.tabs.create({'url': 'html/updated.html'})
		});

		//init context menu
		chrome.storage.sync.get(function(r) {
			const createMenu = {
				"id": "addTo",
				"title": this.vocabulary.add,
				"enabled": false
			};
			chrome.contextMenus.create(createMenu);
			if (JSON.stringify(r) !== "{}") {
				for (const key in r) {
					if (Object.prototype.hasOwnProperty.call(r, key)) {
						this.addSubMenuItem(key, JSON.parse(r[key]).discr);
					}
				}
				chrome.contextMenus.update("addTo", {"enabled": true});
			}
		}.bind(this));
	}

	//handlers
	stash() {
		chrome.tabs.query({}, (tabs) => {
			const obj = {};
			const date = Date.now();
			const ret = [];
			const rem = [];
			for (const tab of tabs) {
				if (tab.url !== "chrome://newtab/") {
					ret.push({
						'url': tab.url,
						'title': tab.title,
						'favIconUrl': tab.favIconUrl
					});
				}
				rem.push(tab.id);
			}
			if (!ret.length) {
				console.error("nothing to stash");
				return;
			}
			obj[date] = JSON.stringify({
				"discr": "",
				"data": ret
			});
			this.addSubMenuItem(date, "");
			chrome.storage.sync.set(obj);
			if (!BackgroundApp.isYaBrowser()) {
				chrome.tabs.create({});
			}
			chrome.tabs.remove(rem);
		});
	}

	save() {
		chrome.tabs.query({}, (tabs) => {
			const obj = {};
			const date = Date.now();
			const ret = [];
			for (const tab of tabs) {
				if (tab.url !== "chrome://newtab/") {
					ret.push({
						'url': tab.url,
						'title': tab.title,
						'favIconUrl': tab.favIconUrl
					});
				}
			}
			if (!ret.length) {
				console.error("nothing to save");
				return;
			}
			obj[date] = JSON.stringify({
				"discr": "",
				"data": ret
			});
			chrome.storage.sync.set(obj);
			this.addSubMenuItem(date, "");
			chrome.runtime.sendMessage({
				"type": "reload",
				"key": date
			});
		});
	}

	static pop(arg) {
		chrome.storage.sync.get(arg, function(r) {
			const obj = JSON.parse(r[arg]);
			for (const tab of obj.data) {
				chrome.tabs.create({"url": tab.url});
			}
			chrome.storage.sync.remove(arg);
			// eslint-disable-next-line no-invalid-this
			chrome.contextMenus.remove(arg, this.checkMaimMenu.bind(this, false));
		});
	}

	static restore(arg) {
		chrome.storage.sync.get(arg, function(r) {
			const obj = JSON.parse(r[arg]);
			for (const tab of obj.data) {
				chrome.tabs.create({"url": tab.url});
			}
		});
	}

	static popAll() {
		chrome.storage.sync.get(function(r) {
			for (const name in r) {
				if (Object.prototype.hasOwnProperty.call(r, name)) {
					const obj = JSON.parse(r[name]);
					// eslint-disable-next-line no-invalid-this
					chrome.contextMenus.remove(name, this.checkMaimMenu.bind(this, false));
					for (const tab of obj.data) {
						chrome.tabs.create({"url": tab.url});
					}
				}
			}
			chrome.storage.sync.clear();
		});
	}

	removeSession(arg) {
		chrome.storage.sync.remove(arg, () => {
			chrome.runtime.sendMessage({"type": "isRemove",
	"key": arg});
		});
		chrome.contextMenus.remove(arg, this.checkMaimMenu.bind(this, false));
	}

	static updateSession(arg) {
		chrome.storage.sync.get(arg.list, function(r) {
			const obj = JSON.parse(r[arg.list]);
			if (typeof arg.insert === "undefined") {
				for (let i = 0; i < obj.data.length; ++i) {
					if (obj.data[i].url === arg.url) {
						obj.data.splice(i, 1);
						break;
					}
				}
			} else {
				obj.data.push(arg.insert);
			}
			if (obj.data.length) {
				const ret = {};
				ret[arg.list] = JSON.stringify(obj);
				chrome.storage.sync.set(ret);
			} else {
				chrome.storage.sync.remove(arg.list);
			}
		});
	}

	static discript(arg) {
		chrome.storage.sync.get([arg.list], function(r) {
			const obj = JSON.parse(r[arg.list]);
			obj.discr = arg.discr;
			const ret = {};
			ret[arg.list] = JSON.stringify(obj);
			chrome.storage.sync.set(ret);
			chrome.contextMenus.update(
	String(arg.list),
			{"title": (function() {
				if (arg.discr.length) {
					return arg.discr;
				}
				const data = new Date(Number.parseInt(arg.list, 10));
				return `${data.getDate()}/${data.getMonth() + 1}/${data.getFullYear()} ${data.getHours()}:${("0" + data.getMinutes()).substr(-2)}`;
			}())}
	);
		});
	}

	//help methods

	addSubMenuItem(id, discr) {
		const createSubMenu = {
			"id": String(id),
			"title": (function() {
				if (discr.length) {
					return discr;
				}
				const data = new Date(Number.parseInt(id, 10));
				return `${data.getDate()}/${data.getMonth() + 1}/${data.getFullYear()} ${data.getHours()}:${("0" + data.getMinutes()).substr(-2)}`;
			}()),
			"parentId": "addTo"
		};
		chrome.contextMenus.create(createSubMenu, this.checkMaimMenu.bind(this, true));
	}

	/**
	 * Check changes of count items.
	 * @param {Boolean} isUp Increment param.
	 * @returns {void}
	 */
	checkMaimMenu(isUp) {
		isUp ? this.counterOfMenuItems++ : this.counterOfMenuItems--;
		chrome.contextMenus.update("addTo", {"enabled": this.counterOfMenuItems > 0});
	}

	static isYaBrowser() {
		return navigator.userAgent.search(/YaBrowser/) > 0;
	}
}

// eslint-disable-next-line no-new
new BackgroundApp();