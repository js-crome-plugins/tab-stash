# tab stash
Плагин для быстрого сохраненмя набора вкладок браузера

[Chrome Store](https://chrome.google.com/webstore/detail/tab-stash/npgpfnolppoefhlncjcgabbanjgibfbj)

[Download v1.0.3](https://gitlab.com/js-crome-plugins/tab-stash/-/archive/v1.0.2/tab-stash-v1.0.3.zip)

[Download v1.0.2](https://gitlab.com/js-crome-plugins/tab-stash/-/archive/v1.0.2/tab-stash-v1.0.2.zip)

[Download v1.0](https://gitlab.com/js-crome-plugins/tab-stash/-/archive/v1.0/tab-stash-v1.0.zip)

[Download Release](https://gitlab.com/js-crome-plugins/tab-stash/-/archive/Release/tab-stash-Release.zip)

# TODO
1. [x] ООП струкрура
2. [ ] Перетаскивание элементов в сессии и самих сессий, сохранение их позиций.
3. [ ] Обеъединение сессий
4. [ ] update list.
5. [ ] Поиск/фильтр по сессиям.
6. [ ] Сохранение открытого и скрытого состояния, кнопка свернуть все.
7. [ ] Экспорт/импорт настроек
8. [x] Remove dialog
9. [ ] В инпуте применять по потере фокуса и нет по esc
10. [ ] Перевод всего.
11. [ ] Примудрости с гугл кип апи.
12. [ ] Оргпнизовать хранение с запоминанием порядка.
13. [ ] Контроль закрепленных вкладок и их состояний.
14. [ ] Вывод ошибок.

# Полезные ссылки
[Про манифест json](https://developer.chrome.com/extensions/manifest)
[drag-and-drop](https://learn.javascript.ru/drag-and-drop)